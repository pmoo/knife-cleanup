Knife-Cleanup
===
```bash
knife cleanup versions <-D>
```

If you run it without --delete (-D) it will show you the versions that would be deleted, but not delete anything. In delete mode it will save first a backup of the version and then proceed to delete it. I've seen various strange situations where knife is not able to download the cookbook version from the server, and be aware that we will skip those cases and there will not be a backup for such corrupted versions. You've been warned. 

Note: this is by no means production ready; I'm using it with success for my needs and hopefully you will find it useful too. Be sure to do a backup your chef server ([knife-backup][knifebackup] before using it, etc. 

## Development

* Source hosted at [GitHub][repo]
* Report issues/questions/feature requests on [GitHub Issues][issues]

Pull requests are very welcome! Ideally create a topic branch for every separate change you make. For example:

1. Fork the repo
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Authors

Created and maintained by [Marius Ducea][mdxp] (<marius.ducea@gmail.com>)

## License

Apache License, Version 2.0 (see [LICENSE][license])

[license]:      https://github.com/mdxp/knife-cleanup/blob/master/LICENSE
[mdxp]:         https://github.com/mdxp
[repo]:         https://github.com/mdxp/knife-cleanup
[issues]:       https://github.com/mdxp/knife-cleanup/issues
[knifebackup]:  https://github.com/mdxp/knife-backup
[chefjenkins]:  https://github.com/mdxp/chef-jenkins